FROM node:14.16.0 as node
WORKDIR /app
COPY . .
RUN npm cache clean --force
RUN npm install -g @angular/cli
RUN npm install
RUN npm run build --prod

FROM nginx:alpine
COPY --from=node /app/dist/berger-paing-leads   /usr/share/nginx/html
EXPOSE 80


