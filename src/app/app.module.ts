import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AuthComponent } from './components/auth/auth.component';
import { MaterialModule } from './material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MainComponent } from './components/main/main.component';
import { DashboardComponent } from './components/main/dashboard/dashboard.component';
import { CreateComponent } from './components/main/create/create.component';
import { CommonModule } from '@angular/common';
import { AgentComponent } from './components/main/agent/agent.component';
import { JobListingComponent } from './components/main/job-listing/job-listing.component';
import { HttpClientModule } from '@angular/common/http';
import { JobsSubListingComponent } from './components/main/job-listing/jobs-sub-listing/jobs-sub-listing.component';
import { JobsActivitiesComponent } from './components/main/job-listing/jobs-activities/jobs-activities.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { InProgressComponent } from './components/main/in-progress/in-progress.component';
import { InProgressActivityComponent } from './components/main/in-progress-activity/in-progress-activity.component';
import { AgentReportsComponent } from './components/main/agent-reports/agent-reports.component';
import { AgentKeysComponent } from './components/main/agent-keys/agent-keys.component';

@NgModule({
  declarations: [
    AppComponent,
    AuthComponent,
    MainComponent,
    DashboardComponent,
    CreateComponent,
    AgentComponent,
    JobListingComponent,
    JobsSubListingComponent,
    JobsActivitiesComponent,
    InProgressComponent,
    InProgressActivityComponent,
    AgentReportsComponent,
    AgentKeysComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MaterialModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    HttpClientModule,
    NgxSpinnerModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
