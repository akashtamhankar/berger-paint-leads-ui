import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class CommonService {

  private ApiUrl = environment.API_URL;
  private AgentKeyAPIUrl = environment.AGENT_KEY_API_URL;
  constructor(private http: HttpClient) { }

  getJObList(data) {
    return this.http.post(this.ApiUrl + 'berger_paint/list-leads/', data)
  }

  downlaodList(data){
    return this.http.post(this.ApiUrl + 'berger_paint/download-leads-csv/', data,  { responseType: 'blob' })
  }

  getJobSubList(params) {
    return this.http.get(this.ApiUrl + 'job-detail/', { params: params });
  }

  getJobActivityList(data) {
    return this.http.post(this.ApiUrl + 'activities/', data);
  }

  getAgentList() {
    return this.http.get(this.ApiUrl + 'list-of-agent/');
  }

  getOverallJobStatus(data) {
    return this.http.get(this.ApiUrl + 'overall-job-status/', { params: data })
  }

  getOverallCampaignStatisctics(data) {
    return this.http.get(this.ApiUrl + "overall-campaign-stats/", { params: data })
  }

  getCampaignDuration(data) {
    return this.http.get(this.ApiUrl + "campaign-duration/", { params: data });
  }

  getInProgressJobList() {
    return this.http.get(this.ApiUrl + "inprogress-job-activity/");
  }

  getVerifiedAgents(data) {
    return this.http.post(this.ApiUrl + "get-verified-stats/", data)
  }

  getAgentKey(agent_name) {
    return this.http.get(`${this.AgentKeyAPIUrl}download-agentkey-files/${agent_name}`, { responseType: 'blob' })
  }
  getCardDetails(data){
    return this.http.post(this.ApiUrl + "berger_paint/analytics/", data)
  }
  getLocalStorageItem(item: string) {
    return localStorage.getItem(item);
  }
}
