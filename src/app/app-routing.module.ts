import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthComponent } from './components/auth/auth.component';
import { CreateComponent } from './components/main/create/create.component';
import { DashboardComponent } from './components/main/dashboard/dashboard.component';
import { MainComponent } from './components/main/main.component';
import { AgentComponent } from './components/main/agent/agent.component';
import { JobListingComponent } from './components/main/job-listing/job-listing.component';
import { InProgressComponent } from './components/main/in-progress/in-progress.component';
import { InProgressActivityComponent } from './components/main/in-progress-activity/in-progress-activity.component';
import { AgentReportsComponent } from './components/main/agent-reports/agent-reports.component';


const routes: Routes = [
  { path: 'auth', component: AuthComponent },
  {
    path: 'main', component: MainComponent, children: [
      { path: 'dashboard', component: DashboardComponent },
      { path: 'create', component: CreateComponent },
      { path: 'agent', component: AgentComponent },
      { path: 'in-progress', component: InProgressComponent },
      { path: 'job-list-details/:id', component: JobListingComponent },
      { path: 'in-progress-activity/:id', component: InProgressActivityComponent },
      { path: 'agent-reports', component: AgentReportsComponent },
      { path: '', redirectTo: "dashboard", pathMatch: "full" },
    ]
  },
  { path: '', redirectTo: "auth", pathMatch: 'full' },
  { path: '**', redirectTo: "auth", pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
