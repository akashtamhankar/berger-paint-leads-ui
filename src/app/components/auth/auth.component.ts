import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import swal from 'sweetalert2';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
  
})
export class AuthComponent implements OnInit {
  usernameFormControl = new FormControl('', [Validators.required]);
  passwordFormControl = new FormControl('', [Validators.required])
  hide = true;

  constructor(public router: Router, private spinner: NgxSpinnerService) { }

  ngOnInit(): void {
  }

  login() {
    this.spinner.show()
    if ((this.usernameFormControl.value == 'bergerpaint' && this.passwordFormControl.value == '3*8kKC?S&M') || (this.usernameFormControl.value == 'bergerpaint1' && this.passwordFormControl.value == '5L4B.nHZe)XgANRP') || (this.usernameFormControl.value == 'bergerpaint2' && this.passwordFormControl.value == 'd4(vdRs5yhB2EK-u') || (this.usernameFormControl.value == 'bergerpaint3' && this.passwordFormControl.value == 'yU3(e9LT{e<xd?h?') || (this.usernameFormControl.value == 'bergerpaint4' && this.passwordFormControl.value == 's-f},E3YEHB4}#"a')) {
      this.spinner.hide();
      this.router.navigate(["/main"]);
      
    }
    else {
      this.spinner.hide();
      swal.fire({
        icon: 'error',
        title: 'Invalid User Credentials !!',
      })
      
    }

  }
}
