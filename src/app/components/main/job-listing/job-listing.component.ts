import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { CommonService } from 'src/app/services/common.service';

@Component({
  selector: 'app-job-listing',
  templateUrl: './job-listing.component.html',
  styleUrls: ['./job-listing.component.scss']
})
export class JobListingComponent implements OnInit {
  jobID: any;
  overallJobStatus = '';
  campaignDuration = "";
  overallCampaignStatistics: any;
  constructor(private common: CommonService, private router: Router, public route: ActivatedRoute, private spinner: NgxSpinnerService) { }

  ngOnInit(): void {
    this.jobID = this.router.url.split("/").pop();
    this.getOverallStatus();
    this.getOverallCampaignStatistics();
    this.getCampaignDuration();
  }

  getOverallStatus() {
    this.spinner.show()
    this.common.getOverallJobStatus({ job_id: this.jobID }).subscribe((res: any) => {
      this.spinner.hide()
      if (res.length == 0) {
        this.overallJobStatus = "--";
      } else {
        this.overallJobStatus = res.job_status;
      }
    }, err => {
      console.log(err);
      this.spinner.hide();
    })
  }

  getOverallCampaignStatistics() {
    this.spinner.show()
    this.common.getOverallCampaignStatisctics({ job_id: this.jobID }).subscribe((res: any) => {
      this.spinner.hide()
      this.overallCampaignStatistics = res;
    }, err => {
      console.log(err);
      this.spinner.hide();
    })
  }

  getCampaignDuration() {
    this.spinner.show();
    this.common.getCampaignDuration({ job_id: this.jobID }).subscribe((res: any) => {
      this.spinner.hide();
      this.campaignDuration = res["Campaign Duration"];
    }, err => {
      console.log(err);
      this.spinner.hide();
    })
  }
}
