import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { CommonService } from 'src/app/services/common.service';

@Component({
  selector: 'app-jobs-activities',
  templateUrl: './jobs-activities.component.html',
  styleUrls: ['./jobs-activities.component.scss']
})
export class JobsActivitiesComponent implements OnInit {
  jobsActivityList: any = [];
  jobID: any;
  constructor(private common: CommonService, private router: Router, public route: ActivatedRoute, private spinner: NgxSpinnerService) { }

  ngOnInit(): void {
    this.jobID = this.router.url.split("/").pop();
    this.getJobActivityLists();
  }

  getJobActivityLists() {
    this.spinner.show();
    this.common.getJobActivityList({ job_id: this.jobID }).subscribe(res => {
      this.spinner.hide();
      this.jobsActivityList = res;
    }, err => {
      this.spinner.hide();
      console.log(err);
    });
  }
}
