import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JobsActivitiesComponent } from './jobs-activities.component';

describe('JobsActivitiesComponent', () => {
  let component: JobsActivitiesComponent;
  let fixture: ComponentFixture<JobsActivitiesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JobsActivitiesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JobsActivitiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
