import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JobsSubListingComponent } from './jobs-sub-listing.component';

describe('JobsSubListingComponent', () => {
  let component: JobsSubListingComponent;
  let fixture: ComponentFixture<JobsSubListingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JobsSubListingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JobsSubListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
