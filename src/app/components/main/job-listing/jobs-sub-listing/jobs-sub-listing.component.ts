import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { CommonService } from 'src/app/services/common.service';

@Component({
  selector: 'app-jobs-sub-listing',
  templateUrl: './jobs-sub-listing.component.html',
  styleUrls: ['./jobs-sub-listing.component.scss']
})
export class JobsSubListingComponent implements OnInit {
  jobsSubList: any = [];
  jobID: any;
  constructor(private common: CommonService, private router: Router, public route: ActivatedRoute, private spinner: NgxSpinnerService) { }


  ngOnInit(): void {
    this.jobID = this.router.url.split("/").pop();
    this.getJobSubLists();
  }

  getJobSubLists() {
    this.spinner.show()
    this.common.getJobSubList({ job_id: this.jobID }).subscribe(res => {
      this.spinner.hide()
      this.jobsSubList = res;
      console.log(res);
    }, err => {
      this.spinner.hide();
    });
  }

}
