import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { Form, FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { CommonService } from '../../../services/common.service';

export interface JobElement {
  total_phone_numbers: string;
  job_id: number;
  total_number_supporting_verified_calls: number;
  user: string;
  agent_id: string;
  started_at: string;
  completed_at: string;
  details: string
}

@Component({
  selector: 'app-agent',
  templateUrl: './agent.component.html',
  styleUrls: ['./agent.component.scss']
})
export class AgentComponent implements OnInit, AfterViewInit {
  displayedColumns: string[] = ['job_id', 'total_phone_numbers', 'total_number_supporting_verified_sms', 'total_phone_number_verified', 'user', 'agent_id', 'started_at', 'completed_at', 'details']; jobList: any = [];
  dataSource = new MatTableDataSource<JobElement>(this.jobList);
  @ViewChild(MatPaginator) paginator: MatPaginator;
  agent: FormControl = new FormControl('');
  start_date: FormControl = new FormControl(new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate() - 7));
  completion_date: FormControl = new FormControl(new Date());
  search_job_id: FormControl = new FormControl('');
  options: any = [];
  records_response: string = "";
  constructor(private commonService: CommonService, public router: Router, public route: ActivatedRoute, private spinner: NgxSpinnerService, private _snackBar: MatSnackBar) { }

  ngOnInit() {
    this.getJobList();
    this.getAgentList();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  getAgentList() {
    this.spinner.show();
    this.commonService.getAgentList().subscribe((res: any) => {
      this.spinner.hide();
      res.forEach(agent => {
        this.options.push(agent.agent_id);
      })
    }, err => {
      this.spinner.hide();
      console.log(err);
      this._snackBar.open("Could not fetch job list", "Close", {
        duration: 3000,
        horizontalPosition: 'right',
        verticalPosition: "top",
        panelClass: ['error-snackbar']
      })
    })
  }

  getJobList() {
    this.spinner.show();
    var data: any = {};
    data.agent_id = this.agent.value;
    data.start_date = new Date(this.start_date.value).toISOString().slice(0, 10);
    data.end_date = new Date(this.completion_date.value).toISOString().slice(0, 10);
    this.commonService.getJObList(data).subscribe((result: any) => {
      this.spinner.hide();
      this.jobList = result;
      this.dataSource.data = this.jobList
    }, err => {
      this.dataSource.data = []
      this.spinner.hide();
      this.records_response = err.error.response
      console.log(err);
      this._snackBar.open("Could not fetch job list", "Close", {
        duration: 3000,
        horizontalPosition: 'right',
        verticalPosition: "top",
        panelClass: ['error-snackbar']
      })
    })
  }


  searchJobDetails() {
    if (this.search_job_id.value && this.search_job_id.value.length >= 6) {
      this.commonService.getJobSubList({ job_id: this.search_job_id.value }).subscribe(res => {
        this.redirectToDetails(this.search_job_id.value);
      }, err => {
        this._snackBar.open("Please enter a valid job ID", "Close", {
          duration: 3000,
          horizontalPosition: 'right',
          verticalPosition: "top",
          panelClass: ['error-snackbar']
        })
      })
    } else {
      this._snackBar.open("Please enter a valid job ID", "Close", {
        duration: 3000,
        horizontalPosition: 'right',
        verticalPosition: "top",
        panelClass: ['error-snackbar']
      })
    }
  }

  redirectToDetails(element) {
    this.router.navigate(['./job-list-details', element], { relativeTo: this.route })
  }

}