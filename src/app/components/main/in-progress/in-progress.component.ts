import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { CommonService } from 'src/app/services/common.service';

export interface JobElement {
  total_phone_numbers: string;
  job_id: number;
  total_number_supporting_verified_calls: number;
  user: string;
  agent_id: string;
  started_at: string;
  completed_at: string;
  details: string
}

@Component({
  selector: 'app-in-progress',
  templateUrl: './in-progress.component.html',
  styleUrls: ['./in-progress.component.scss']
})
export class InProgressComponent implements OnInit {
  displayedColumns: string[] = ['job_id', 'agent_id', 'job_data', 'exception_data', 'exception_message', 'status_message', 'state', 'date_created', 'details'];
  dataSource = new MatTableDataSource<JobElement>([]);
  @ViewChild(MatPaginator) paginator: MatPaginator;
  options: any = [];
  records_response: string = "";
  constructor(private commonService: CommonService, public router: Router, public route: ActivatedRoute, private spinner: NgxSpinnerService, private _snackBar: MatSnackBar) { }

  ngOnInit() {
    this.getInProgressJobList();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  getInProgressJobList() {
    this.spinner.show();
    this.commonService.getInProgressJobList().subscribe((result: any) => {
      this.spinner.hide();
      this.dataSource.data = result;
    }, err => {
      this.records_response = err.error.response
      this.dataSource.data = []
      this.spinner.hide();
      this._snackBar.open("Could not fetch job list", "Close", {
        duration: 3000,
        horizontalPosition: 'right',
        verticalPosition: "top",
        panelClass: ['error-snackbar']
      })
    })
  }

  redirectToDetails(element) {
    this.router.navigate(['./in-progress-activity', element], { relativeTo: this.route })
  }
}
