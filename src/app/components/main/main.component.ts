import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/services/common.service';
import { AgentKeysComponent } from './agent-keys/agent-keys.component';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
  current_section = "";
  constructor(public router: Router, public dialog: MatDialog, public common: CommonService) {
    router.events.subscribe((val: any) => {
      this.changeCurrentSection(val.url)
    });
  }

  ngOnInit(): void {
    // if (!localStorage.getItem("vsm_username") || !localStorage.getItem("vsm_password")) {
    //   this.router.navigate(["/auth"]);
    // }
    this.changeCurrentSection(this.router.url)
  }

  changeCurrentSection(url) {
    if (url) {
      if (url == '/main/dashboard') this.current_section = 'Dashboard';
      if (url == '/main/agent') this.current_section = 'Agent';
      if (url == '/main/in-progress') this.current_section = 'In Progress';
      if (url.includes('/job-list-details')) this.current_section = 'Job_details';
      if (url.includes('/in-progress-activity')) this.current_section = 'In Progress Activity';
      if (url.includes('/agent-reports')) this.current_section = 'Agent Reports';
    }
  }

  openAgentKeyDownload() {
    const dialogRef = this.dialog.open(AgentKeysComponent, {
      minWidth: '500px'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

  logOut() {
    localStorage.removeItem("vsm_username");
    localStorage.removeItem("vsm_password");
    this.router.navigate(['/auth'])
  }
}
