import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { CommonService } from 'src/app/services/common.service';

@Component({
  selector: 'app-agent-reports',
  templateUrl: './agent-reports.component.html',
  styleUrls: ['./agent-reports.component.scss']
})
export class AgentReportsComponent implements OnInit {
  agent: FormControl = new FormControl('');
  start_date: FormControl = new FormControl(new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate() - 7));
  options: any = [];
  agent_verified_stats: any;
  completion_date: FormControl = new FormControl(new Date());
  constructor(private commonService: CommonService, public router: Router, public route: ActivatedRoute, private spinner: NgxSpinnerService, private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.getAgentList();
  }

  getVerifiedAgents() {
    if (this.agent.value) {
      this.commonService.getVerifiedAgents({
        start_date: new Date(this.start_date.value).toISOString().slice(0, 10),
        end_date: new Date(this.completion_date.value).toISOString().slice(0, 10),
        agent_id: this.agent.value
      }).subscribe(res => {
        this.agent_verified_stats = res;
        this._snackBar.open("Agent records have been fetched.", "Close", {
          duration: 3000,
          horizontalPosition: 'right',
          verticalPosition: "top",
          panelClass: ['success-snackbar']
        })
      }, err => {
        console.log(err);
        this._snackBar.open(err.error.response, "Close", {
          duration: 3000,
          horizontalPosition: 'right',
          verticalPosition: "top",
          panelClass: ['error-snackbar']
        })
      })
    } else {
      this._snackBar.open("Please select an agent.", "Close", {
        duration: 3000,
        horizontalPosition: 'right',
        verticalPosition: "top",
        panelClass: ['error-snackbar']
      })
    }
  }

  getAgentList() {
    this.spinner.show();
    this.commonService.getAgentList().subscribe((res: any) => {
      this.spinner.hide();
      res.forEach(agent => {
        this.options.push(agent.agent_id);
      })
      this.agent.setValue(this.options[0])
      this.getVerifiedAgents()
    }, err => {
      this.spinner.hide();
      console.log(err);
      this._snackBar.open("Could not fetch job list", "Close", {
        duration: 3000,
        horizontalPosition: 'right',
        verticalPosition: "top",
        panelClass: ['error-snackbar']
      })
    })
  }
}
