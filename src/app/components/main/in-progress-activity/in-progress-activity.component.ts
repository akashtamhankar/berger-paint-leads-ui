import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { CommonService } from 'src/app/services/common.service';

@Component({
  selector: 'app-in-progress-activity',
  templateUrl: './in-progress-activity.component.html',
  styleUrls: ['./in-progress-activity.component.scss']
})
export class InProgressActivityComponent implements OnInit {

  jobID: any;
  overallJobStatus = '';
  campaignDuration = "";
  overallCampaignStatistics: any;
  constructor(private common: CommonService, private router: Router, public route: ActivatedRoute, private spinner: NgxSpinnerService) { }

  ngOnInit(): void {
    this.jobID = this.router.url.split("/").pop();
    this.getOverallStatus();
    this.getOverallCampaignStatistics();
    this.getCampaignDuration();
  }

  getOverallStatus() {
    this.spinner.show()
    this.common.getOverallJobStatus({ job_id: this.jobID }).subscribe((res: any) => {
      this.spinner.hide()
      if (res.length == 0) {
        this.overallJobStatus = "--";
      } else {
        this.overallJobStatus = res.job_status;
      }
    }, err => {
      console.log(err);
      this.spinner.hide();
    })
  }

  getOverallCampaignStatistics() {
    this.spinner.show()
    this.common.getOverallCampaignStatisctics({ job_id: this.jobID }).subscribe((res: any) => {
      this.spinner.hide()
      this.overallCampaignStatistics = res;
    }, err => {
      console.log(err);
      this.spinner.hide();
    })
  }

  getCampaignDuration() {
    this.spinner.show();
    this.common.getCampaignDuration({ job_id: this.jobID }).subscribe((res: any) => {
      this.spinner.hide();
      this.campaignDuration = res["Campaign Duration"];
    }, err => {
      console.log(err);
      this.spinner.hide();
    })
  }
}
