import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InProgressActivityComponent } from './in-progress-activity.component';

describe('InProgressActivityComponent', () => {
  let component: InProgressActivityComponent;
  let fixture: ComponentFixture<InProgressActivityComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InProgressActivityComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InProgressActivityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
