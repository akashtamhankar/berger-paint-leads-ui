import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CommonService } from 'src/app/services/common.service';
import { saveAs } from 'file-saver';

@Component({
  selector: 'app-agent-keys',
  templateUrl: './agent-keys.component.html',
  styleUrls: ['./agent-keys.component.scss']
})
export class AgentKeysComponent implements OnInit {
  agent_name: FormControl = new FormControl('');
  constructor(private commonService: CommonService, private _snackBar: MatSnackBar, public dialogRef: MatDialogRef<AgentKeysComponent>) { }

  ngOnInit(): void {
  }

  downloadAgentKey() {
    if (this.agent_name.value == '') {
      return this._snackBar.open("Please enter a name and try again.", "Close", {
        duration: 3000,
        horizontalPosition: 'right',
        verticalPosition: "top",
        panelClass: ['error-snackbar']
      })
    }

    this.commonService.getAgentKey(this.agent_name.value).subscribe((res: any) => {
      saveAs(res, this.agent_name.value)
      this.agent_name.setValue('')
      this.dialogRef.close();
    }, err => {
      console.log(err);

    })
  }

}
