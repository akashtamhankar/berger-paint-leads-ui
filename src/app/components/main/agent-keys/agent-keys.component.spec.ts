import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AgentKeysComponent } from './agent-keys.component';

describe('AgentKeysComponent', () => {
  let component: AgentKeysComponent;
  let fixture: ComponentFixture<AgentKeysComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AgentKeysComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AgentKeysComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
