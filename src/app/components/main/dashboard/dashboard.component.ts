import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { Form, FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { CommonService } from '../../../services/common.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { saveAs } from 'file-saver';
import * as moment from 'moment';

export interface JobElement {
  total_phone_numbers: string;
  job_id: number;
  total_number_supporting_verified_calls: number;
  user: string;
  agent_id: string;
  started_at: string;
  completed_at: string;
  details: string
}

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements AfterViewInit, OnInit {
  displayedColumns: string[] = ['name', 'status', 'mobile_number', 'email_id', 'rating','created_at', 'updated_at', 'reference_id', 'enquiry_qualified', 'looking_for','description', 'paint_type','address','pincode','carpet_area','preferred_language']; jobList: any = [];
  dataSource = new MatTableDataSource<JobElement>(this.jobList);
  @ViewChild(MatPaginator) paginator: MatPaginator;
  start_date: FormControl = new FormControl(new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate() - 7));
  completion_date: FormControl = new FormControl(new Date());
  search_job_id: FormControl = new FormControl('');
  records_response: string = "";card_data:any;
  constructor(private commonService: CommonService, public router: Router, public route: ActivatedRoute, private spinner: NgxSpinnerService, private _snackBar: MatSnackBar) { }

  ngOnInit() {
    this.getJobList()
    this.getCardDetails()
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  getJobList() {
    this.spinner.show();
    var data: any = {};
    data.start_date = moment(this.start_date.value).format('YYYY-MM-DD');
    data.end_date = moment(this.completion_date.value).format('YYYY-MM-DD');

    this.commonService.getJObList(data).subscribe((result: any) => {
      this.spinner.hide();
      console.log(result)
      this.jobList = result;
      this.dataSource.data = this.jobList;
    }, err => {
      this.dataSource.data = []
      this.records_response = err.error.response
      console.log(err);
      this.spinner.hide();
      this._snackBar.open("Could not fetch the data", "Close", {
        duration: 3000,
        horizontalPosition: 'right',
        verticalPosition: "top",
        panelClass: ['error-snackbar']
      })
    })
  }
  download(){
    this.spinner.show();
    var data: any = {};
    data.start_date = moment(this.start_date.value).format('YYYY-MM-DD');
    data.end_date = moment(this.completion_date.value).format('YYYY-MM-DD');

    this.commonService.downlaodList(data).subscribe((result: any) => {
      this.spinner.hide();
      // console.log(result)
      saveAs(result, `Lead_records_${moment().format('DD-MM-YYYY')}.csv`, {type: "text/csv"})
      
    }, err => {
      this.dataSource.data = []
      // this.records_response = err.error.response
      console.log(err);
      this.spinner.hide();
      this._snackBar.open("Could not fetch job list", "Close", {
        duration: 3000,
        horizontalPosition: 'right',
        verticalPosition: "top",
        panelClass: ['error-snackbar']
      })
    })


  }
  getCardDetails(){
    this.spinner.show();
    var data: any = {};
    data.start_date = moment(this.start_date.value).format('YYYY-MM-DD');
    data.end_date = moment(this.completion_date.value).format('YYYY-MM-DD');

    this.commonService.getCardDetails(data).subscribe((result: any) => {
      this.spinner.hide();
      console.log(result)
      this.card_data = result;
    }, err => {
      console.log(err);
      this.spinner.hide();
      this._snackBar.open("Could not fetch the data", "Close", {
        duration: 3000,
        horizontalPosition: 'right',
        verticalPosition: "top",
        panelClass: ['error-snackbar']
      })
    })
  }



  redirectToDetails(element) {
    this.router.navigate(['./job-list-details', element], { relativeTo: this.route })
  }


}